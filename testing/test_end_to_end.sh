#!/usr/bin/env bash
# This script simply test end-to-end behaviou by downloading the init from the
# repository. Therefore changes in local repository are not reflected by this
# until pushed.

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "${SCRIPT_DIR}" || exit 1

docker build \
  --build-arg USERNAME="$USER" \
  --build-arg USER_UID="$UID" \
  --tag init_tester \
  . || exit 1


docker run \
  --rm \
  -it \
  init_tester \
  /bin/bash -c "/bin/bash <(curl -s https://gitlab.com/marcobox/init/-/raw/main/init.sh); exec /bin/zsh -l"
