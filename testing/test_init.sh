#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "${SCRIPT_DIR}" || exit 1

docker build \
  --build-arg USERNAME="$USER" \
  --build-arg USER_UID="$UID" \
  --tag init_tester \
  . || exit 1


docker run \
  --volume "${SCRIPT_DIR}/../init.sh:/init.sh:ro" \
  --rm \
  -it \
  init_tester
