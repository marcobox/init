#!/bin/bash

echo "Please provide app token for authentication with privare repository"
read -sp 'Password: ' -r TOKEN || exit 1

CHEZMOI_DIR="${HOME}/.local/share/chezmoi"
cd "$(mktemp -d)" || exit 1

sh -c "$(curl -fsLS get.chezmoi.io)" -- init --purge-binary "https://marcobox:${TOKEN}@gitlab.com/marcobox/dotfiles.git"

echo "Finished initializing dotfile repository. Starting environment configuration..."

"${CHEZMOI_DIR}/init.sh"
