# Init

Initialize the environment to allow turnup of a new linux environment.

Simply run the following command (not `curl` and `sudo` acces are required):

```sh
/bin/bash <(curl -s https://gitlab.com/marcobox/init/-/raw/main/init.sh)
```

To retrieve the repository you will need the access token that will be requested as input when you run the script.

## Testing

To test the init script you can use the `test_unit.sh` that setup an empty Ubuntu Docker container and runs the script within.

## Steam Deck VNC

If you want to initialize VNC installation for the steam deck run the following command:

```sh
/bin/bash <(curl -s https://gitlab.com/marcobox/init/-/raw/main/vnc_install.sh)
```
